"""Web views."""

import datetime
import email.utils
import http.client
import math
import typing

import flask

__all__ = [
	"web_blueprint",
	"view"
]

web_blueprint = flask.Blueprint(
	"web",
	__name__
)


def render_meeting(
	start_time_timestamp: float,
	end_time: datetime.datetime,
	total_seconds_passed: float,
	total_seconds_passed_delta: datetime.timedelta,
	start_time: datetime.datetime,
	meeting_name: str,
	use_formatdate: bool = True
) -> typing.Dict[str, typing.Any]:
	return {
		"starts": (
			email.utils.formatdate(start_time_timestamp)
			if use_formatdate
			else start_time
		),
		"ends": (
			email.utils.formatdate(end_time.timestamp())
			if use_formatdate
			else end_time
		),
		"seconds_passed": total_seconds_passed,
		"formatted_seconds_passed": total_seconds_passed_delta,
		"starts_before_passed_seconds": email.utils.formatdate(
			(
				start_time
				- total_seconds_passed_delta
			).timestamp()
		),
		"name": meeting_name,
		"seconds_overriden": False
	}


@web_blueprint.route("/")
def view() -> typing.Tuple[flask.Response, int]:
	current_meeting = None
	past_meetings = []

	total_seconds_passed = 0

	for meeting in flask.current_app.config["MEETINGS"]: # Assume chronological order
		if "seconds_override" in meeting:
			past_meetings.append({
				"seconds_passed": meeting["seconds_override"],
				"formatted_seconds_passed": datetime.timedelta(seconds=meeting["seconds_override"]),
				"name": meeting["name"],
				"seconds_overriden": True
			})

			continue

		start_time = datetime.datetime.fromisoformat(meeting["starts"])
		end_time = datetime.datetime.fromisoformat(meeting["ends"])

		current_time = datetime.datetime.now(datetime.timezone.utc)
		start_time_timestamp = start_time.timestamp()

		if start_time < current_time:
			if current_time > end_time:
				total_seconds_passed += math.ceil(end_time.timestamp() - start_time_timestamp)
				total_seconds_passed_delta = datetime.timedelta(seconds=total_seconds_passed)

				past_meetings.append(
					render_meeting(
						start_time_timestamp,
						end_time,
						total_seconds_passed,
						total_seconds_passed_delta,
						start_time,
						meeting["name"],
						False
					)
				)
			else: # Before or equal to
				total_seconds_passed += math.ceil(current_time.timestamp() - start_time_timestamp)
				total_seconds_passed_delta = datetime.timedelta(seconds=total_seconds_passed)

				current_meeting = render_meeting(
					start_time_timestamp,
					end_time,
					total_seconds_passed,
					total_seconds_passed_delta,
					start_time,
					meeting["name"]
				)

				break

	return flask.render_template(
		"web.html",
		meeting=current_meeting,
		past_meetings=past_meetings
	), http.client.OK
